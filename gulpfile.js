var gulp = require('gulp');
var jshint = require('gulp-jshint');

gulp.task('default', function() {
  console.log('Hello friends!');
});

gulp.task('lint:js', function() {
  gulp.src('src/js/**/*.js')
    .pipe(jshint());
});
