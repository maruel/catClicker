# Cat Clicker

## Description
Petit jeu qui compte le nombre de clique sur une image de chat.

## Installation
1. Copier le `.env.exemple` en `.env`
1. Changer les informations d'environnement dans `.env`
1. Installer les dependencies NPM `npm install`

## Dependencies
* detenv - https://github.com/motdotla/dotenv
